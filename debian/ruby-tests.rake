require 'gem2deb/rake/spectask'

task :before do
  File.rename("./spec/shared/", "./spec/shared.backup/")
  File.rename("./spec/support/", "./spec/support.backup/")
end

task :after do
  File.rename("./spec/shared.backup/", "./spec/shared/")
  File.rename("./spec/support.backup/", "./spec/support/")
end

Gem2Deb::Rake::RSpecTask.new(:spec) do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end

if ENV["AUTOPKGTEST_TMP"]
  task :default => [:before, :spec, :after]
else
  task :default => :spec
end
